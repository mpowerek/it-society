<?php
require_once "Strona.php";
$Strona = new Strona();
define("domena", "localhost");

if (!isset($_SESSION['autologin']) || @$_SESSION['User']->Get_rank() != 3)
    {
      header('Location: index');
      exit();
    }
    $_GET['section'] = '0';

    if(isset($_POST['id']))
    {
      $Strona->DB->Update_Dbfromtablebyid('users','rank', $_POST['setrank'] , $_POST['id']);
      session_destroy(session_id());
    }
    else if(isset($_POST['delete']))
      $Strona->DB->Delete_fromtablebyid('users', $_POST['delete']);
?>
  <!DOCTYPE html>
  <html lang="pl_PL">

  <head>
    <link href="https://fonts.googleapis.com/css?family=Alegreya+Sans+SC" rel="stylesheet">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>IT SOCEITY
    </title>
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
      crossorigin="anonymous">
    <script type="text/javascript">
  var js_variable = <?php echo json_encode($_SESSION['autologin']); ?>;
if(js_variable==false)
{
  var x = document.referrer;    
  x = x.replace("http://", "");
  var i = x.indexOf ("/"); 
  x = x.substr(0,i);
  if (x != "<?php echo domena; ?>")
    window.location.href = "index";
}    
    function scroll_to(selector) {
      $('html,body').animate({
        scrollTop: $(selector).offset().top
      }, 1000);
      return false;
    }
    </script>
  </head>

  <body>
    <div class="main">
      <nav class="navbar navbar-default" role="navigation" style="background: white; position: fixed; width:100%;border-radius:0px; margin-top: -80px; z-index: 999999999999999999999999;">
        <div class="container" style="margin-top: 15px;">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
              <span class="sr-only">Rozwiń nawigację
              </span>
              <span class="icon-bar">
              </span>
              <span class="icon-bar">
              </span>
              <span class="icon-bar">
              </span>
            </button>
            <a class="navbar-brand" href="logged">
              <img class="img-responsive img-logo" src="img/logo.png">
            </a>
          </div>
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right" style="background: white; z-index: 9999;    margin-top: -2px;">
              <li>
                <a href="logged">
                  <i class="fa fa-home" aria-hidden="true">
                  </i> Strona Główna
                </a>
              </li>
              <li class="dropdown">             
                <a href="#" class="zaloguj" data-toggle="dropdown"><?php echo $_SESSION['User']->Get_login(); ?>
                  <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" role="menu" style="padding: 20px;font-family: sans-serif; font-weight: bold;">
                  <?php
                                    echo 'Witaj, '. $_SESSION['User']->Get_imie().' '.$_SESSION['User']->Get_nazwisko().'!<br />';
                                    echo 'Twój e-mail: '. $_SESSION['User']->Get_email().'<br />';
                  ?>
                </ul>
              </li>
              <li>
                <a href="logged">
                <form method="post">
                <button style="background: white; border:0;">Wyloguj</button>
                <input type="hidden" name="logout" value="1" />
                </form>
                <?php
                if(@$_POST['logout'])
                    $Strona->Wyloguj();
                ?>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    <div class="container" style="margin-top: 60px; background: white; padding:0; font-family: sans-serif;">
    <div style="background: #828282;font-size: 1.2em; padding: 15px;" class="menu-section">
    <a href="logged" class="menu-section">Strona Główna</a>
    <a href="admin" class="menu-section menu-active">Użytkownicy</a>
      </div>
      <div style="padding: 15px 0px;">
            <div style="padding: 15px;background: #c71234; color: white;">
            <form style="margin:0;padding:0;" method="post">Fraza <input style="color: black;" type="text" name="wartosc"/> szukaj po:
              <select name="poczym" style="color:black;">
                      <option value="id">id</option>
                      <option value="login">login</option>
                      <option value="email">e-mail</option>
                    </select>
            <button style="color:black;" name="okej" type="submit">wyszukaj</button>
            <!-- <button style="float: right;background: #4caf50;" class="btn" name="zatwierdz">Zatwierdź</button> -->
            </form>
            </div>
        <div style="padding: 5px;background: #f7f7f7;">
          <div class='container'>
            <div class='row' style='border-bottom: 1px solid #D5DDE5;border-top: 1px solid #D5DDE5;padding:10px;'>
              <div class='col-lg-1' >id:</div>
              <div class='col-lg-2' >login:</div>
              <div class='col-lg-2' >e-mail:</div>
              <div class='col-lg-2' >ranga:</div>
              <div class='col-lg-2' >imie:</div>
              <div class='col-lg-2' >nazwisko:</div>
              <div class='col-lg-1' >operacje:</div>
            </div>
<?php
    if(isset($_POST['okej']))
    {
      $liczbastron = $Strona->Get_usersbyid($_POST['wartosc'],$_POST['poczym']);
    }
    else
      $liczbastron = $Strona->Get_usersbyid() ;
?>
           </div>
        </div>
<?php
        $Strona->Get_pager($liczbastron);
?>
      </div>
</div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js">
    </script>
    <script src="js/bootstrap.min.js">
    </script>
  </body>
  </html>