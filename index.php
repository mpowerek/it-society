<?php
ob_start();
require_once "Strona.php";
$Strona = new Strona();
if (@$_SESSION['autologin'])
    {
      header('Location: logged');
      exit();
    }
?>
  <!DOCTYPE html>
  <html lang="pl_PL">
  <head>
    <link href="https://fonts.googleapis.com/css?family=Alegreya+Sans+SC" rel="stylesheet">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>IT SOCEITY
    </title>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
      crossorigin="anonymous">
    <script type="text/javascript">
      <?php
    if(isset($_SESSION['blad-zaloguj']))
      {
      ?>
      $(document).ready(function () {
        $("#logowanie").addClass('open');
      });
      <?php
    }
    else if(isset($_SESSION['blad-zarejestruj']))
    {
      ?>
      $(document).ready(function () {
        $("#rejestracja").modal();
      });
      <?php
    }
?>
      function scroll_to(selector) {
        $('html,body').animate({
          scrollTop: $(selector).offset().top
        }, 1000);
        return false;
      }
    </script>
  </head>
<style>
</style>
<body style="background:white;">
  <div class="main">
    <nav class="navbar navbar-default" role="navigation" style="background: white; position: fixed; width:100%;border-radius:0px; margin-top: -80px; z-index: 9999;">
      <div class="container" style="margin-top: 15px;">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
              <span class="sr-only">Rozwiń nawigację
              </span>
              <span class="icon-bar">
              </span>
              <span class="icon-bar">
              </span>
              <span class="icon-bar">
              </span>
            </button>
            <a class="navbar-brand" href="index">
              <img class="img-responsive img-logo" src="img/logo.png">
            </a>
          </div>
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right" style="background: white; z-index: 1;    margin-top: -2px;">
              <li>
                <a href="index">
                  <i class="fa fa-home" aria-hidden="true">
                  </i> Strona Główna
                </a>
              </li>
              <li class='dropdown' id='logowanie'>
                <a class="zaloguj" href="#" class="dropdown-toggle" data-toggle="dropdown">Logowanie
                  <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" role="menu" style="padding: 20px;">
                  <form class="login-form" method="post" action="">
                    <input type="text" placeholder="Login" id="login" name="login" maxlength="32" />
                    <input type="password" placeholder="Hasło" id="password" name="password" maxlength="64" />
                    <input type="checkbox" id="autologin" name="autologin" checked style="float: left; width: 15%;" />
                    <label for="autologin" style="float: left; margin-top: -2px;">Zapamiętaj Mnie!</label>
                    <input type="hidden" name="zaloguj" value="1" />
                    <br>
                    <br>
                    <br>
                    <font color="red">
                      <?php
                     if (isset($_SESSION['blad-zaloguj'])) 
                     {
                      echo $_SESSION['blad-zaloguj'];
                      unset($_SESSION['blad-zaloguj']);
                     }
                     ?>
                    </font>
                    <button class="btn">Zaloguj</button>
                  </form>
                </ul>
              </li>
              <li>
                <a href="#" data-toggle="modal" class="zaloguj" data-target=".modal">Zarejestruj się!</a>
                <div class='modal fade' id='rejestracja' tabindex='-1' role='dialog' aria-labelledby='myLargeModalLabel'>
                  <form class="login-form" method="post" action="">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content" style="margin-top: 150px; text-align: left; border-radius: 0px; padding: 15px;">
                        <input type="text" placeholder="nazwa użytkownika" maxlength="32" id="register_login" name="register_login" />
                        <input type="password" placeholder="hasło" maxlength="64" id="register_password" name="register_password" />
                        <input type="password" placeholder="Wpisz ponownie hasło" maxlength="64" id="register_password2" name="register_password2"
                        />
                        <input type="text" placeholder="imię" maxlength="32" id="register_name" name="register_name" />
                        <input type="text" placeholder="nazwisko" maxlength="32" id="register_surname" name="register_surname" />
                        <input type="text" placeholder="e-mail" maxlength="62" id="register_email" name="register_email" />
                        <input type="checkbox" id="regulamin" name="regulamin" style="float: left; width: 15%;" />
                        <label for="regulamin" style="float: left; margin-top: -2px;">Akceptuję regulamin!</label>
                        <center>
                          <div class="g-recaptcha" data-sitekey="6LeFM0gUAAAAAIfLnoFaME-PWnOwAznvzL21SvOu"></div>
                        </center>
                        <input type="hidden" name="register" value="1" />
                        <br>
                        <br>
                        <font color="red">
                          <?php
                          if (isset($_SESSION['blad-zarejestruj'])) 
                          {
                            echo $_SESSION['blad-zarejestruj'];
                            unset($_SESSION['blad-zarejestruj']);
                          }
                          ?>
                        </font>
                        <button class="btn" style="margin-top: 30px;">Zarejestruj</button>
                      </form>
                      <?php
                          if (@$_POST['zaloguj'] == 1)
                          {
                              $login = mysqli_real_escape_string($Strona->DB->get_polaczenie(),htmlentities($_POST['login']));
                              $Strona->Zaloguj($login);
                          }
                          else if (@$_POST['register'] == 1)
                          {
                              $login = mysqli_real_escape_string($Strona->DB->get_polaczenie(),htmlentities($_POST['register_login']));
                              $imie = mysqli_real_escape_string($Strona->DB->get_polaczenie(),htmlentities($_POST['register_name']));
                              $nazwisko = mysqli_real_escape_string($Strona->DB->get_polaczenie(),htmlentities($_POST['register_surname']));
                              $email = $_POST['register_email'];
                              $Strona->Zarejestruj($login,$email,$imie,$nazwisko);
                          }
                          if (isset($_SESSION['blad']))
                              echo $_SESSION['blad'];
                      ?>
                    </div>
                  </div>
                </div>
              </ul>
            </li>
          </ul>
        </div>
      </div>
     </nav>
      <section style="margin-top: 60px; " id="what_is_this" class="page1">
        <div style="position:absolute;z-index:999;color:white;width: 100%;">
          <div class="container bluur">
            <h1 class="h3-border-colored">Czym jest IT Society...</h1>
            <h3 class="h3-border">IT Society jest projektem stworzonym przez uczniów Technikum im. Stefana Bieszka w Chojnicach na konkurs organizowany
              przez Pomorski Urząd Marszałkowski, który ma na celu rozwinięcie branży IT w danym regionie. Głównym celem
              tego projektu jest zrzeszenie całej społeczności IT w jednym gronie, tak aby informatycy i laicy, mogli znaleźć
              tutaj odpowiedzi na swoje pytania oraz dzielić się swoim zdaniem na dany temat. Użytkownicy forum mogą również
              dzielić się swoimi projektami i liczyć na ocenę oraz komentarz innych osób na tle forum. Projekt ten, ma również
              za zadanie utworzenie miejsca w tym gronie dla pracodawców, którzy mogliby znaleźć swoich ewentualnych przyszłych
              pracowników, a informatycy swoje przyszłe miejsce pracy.</h3>
          </div>
        </div>
        <video autoplay muted loop id="myVideo">
          <source src="wideo.mp4" type="video/mp4">
        </video>
      </section>
      <div class="nazwa"></div>
      <div class="nazwa"></div>
    </div>
    <section class="bg-white" style="margin-top: -50px;" class="page2">
      </div>
      <div style="position:absolute;z-index:999;color:white;width: 100%;margin-top:100px;">
        <div class="container joboffer" style="color: black;">
          <div style="float:left;width:100%;">
            <h3>Osoby szukające pracy:</h3>
            <br>
            <div class="jobimages">
            <?php
              $sql  =  "SELECT * FROM `looking` ORDER BY `dataedycji` DESC" ;
              $ile=0;
              if ( $result  =  mysqli_query ( $Strona->DB->get_polaczenie() , $sql )) 
              {
                  if ( mysqli_num_rows ( $result ) >  0 ) 
                  {
                      while ( $wiersz  =  mysqli_fetch_array ( $result )) 
                      {
                        if($FromTableById = $Strona->DB->Get_valuefromtablebyid('users',$wiersz['iduser'],'id'))
                        {
                          $ile++;
                          if($ile <= 14)
                          echo "<img src='".$FromTableById['avatar']."' style='min-width: 80px; min-height: 80px; max-width: 80px; max-height: 80px; float: left; border-radius: 40px; padding: 3px; border: 1px solid #D5DDE5;'>";
                        }
                        else
                        {
                          echo "Nie można pobrać informacji o użytkowniku!";
                        }
                      }
                      mysqli_free_result($result);
                  } 
                  else 
                      echo  "Brak Postów!" ;
              }
              else
                echo  "BŁĄD: nie można wykonać zapytania!." . mysqli_error ( $Strona->DB->get_polaczenie() );
              $ile-=14; 
              ?>
          </div>
           </div>
           <?php
            if($ile > 0)
            echo "<h4 style='float:right;'>Oraz ".$ile." innych użytkowników więcej</h4>";
          ?>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <div style="float:left;width:100%;">
            <h3>Osoby oferujące pracę:</h3>
            <br>
            <div class="jobimages">
            <?php
              $sql  =  "SELECT * FROM `jobs` ORDER BY `dataedycji` DESC" ;
              $ile=0;
              if ( $result  =  mysqli_query ( $Strona->DB->get_polaczenie() , $sql )) 
              {
                  if ( mysqli_num_rows ( $result ) >  0 ) 
                  {
                      while ( $wiersz  =  mysqli_fetch_array ( $result )) 
                      {
                        if($FromTableById = $Strona->DB->Get_valuefromtablebyid('users',$wiersz['iduser'],'id'))
                        {
                          $ile++;
                          if($ile <= 14)
                          echo "<img src='".$FromTableById['avatar']."' style='min-width: 80px; min-height: 80px; max-width: 80px; max-height: 80px; float: left; border-radius: 40px; padding: 3px; border: 1px solid #D5DDE5;'>";
                        }
                        else
                        {
                          echo "Nie można pobrać informacji o użytkowniku!";
                        }
                      }
                      mysqli_free_result($result);
                  } 
                  else 
                      echo  "Brak Postów!" ;
              }
              else
                echo  "BŁĄD: nie można wykonać zapytania!." . mysqli_error ( $Strona->DB->get_polaczenie() );
              $ile-=14; 
              ?>
            </div>
          </div>
          <?php
          if($ile > 0)
          echo "<h4 style='float:right;'>Oraz ".$ile." innych użytkowników więcej</h4>";
          ?>
    </section>
    <div class="nazwa"></div>
    <div class="nazwa"></div>
    <section class="bg-1"><div style="margin-top:150px;"><div style="" class="container">
    </div>
    </section>
    <div style="padding: 20px;" class="container">    
      <p align="right">Created by &copy; Igor Leszczyński, Wojciech Wardyn. All rights reserved!</p>     
    </div>
    <script src="js/bootstrap.min.js">
    </script>
  </body>
  </html>