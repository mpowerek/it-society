<?php
ob_start();
require_once "Strona.php";
$Strona = new Strona();
define("domena", "localhost");
if (!isset($_SESSION['autologin']))
    {
      header('Location: index');
      exit();
    }
    if(isset($_POST['upload']))
    {
        $check = getimagesize($_FILES["image"]["tmp_name"]);
        if($check !== false) 
        {
          if($_SESSION['User']->Get_avatar() != 'avatars/avatar.jpg')
          {
            unlink($_SESSION['User']->Get_avatar());
          }
          $Strona->DB->Put_avatarintodb($_FILES['image']['name']);
        } else {
            $_SESSION['blad'] = "Plik nie jest obrazem!";
        }
    }
?>
  <!DOCTYPE html>
  <html lang="pl_PL">
  <head>
    <link href="https://fonts.googleapis.com/css?family=Alegreya+Sans+SC" rel="stylesheet">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>IT SOCEITY
    </title>
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
      crossorigin="anonymous">
    <script type="text/javascript">
  var js_variable = <?php echo json_encode($_SESSION['autologin']); ?>;
  if(js_variable==false)
  {
    var x = document.referrer;    
    x = x.replace("http://", "");
    var i = x.indexOf ("/"); 
    x = x.substr(0,i);
    if (x != "<?php echo domena; ?>")
      window.location.href = "index";
  }
    function scroll_to(selector) {
      $('html,body').animate({
        scrollTop: $(selector).offset().top
      }, 1000);
      return false;
    }
    </script>
  </head>
  <body>
    <div class="main">
      <nav class="navbar navbar-default" role="navigation" style="background: white; position: fixed; width:100%;border-radius:0px; margin-top: -80px; z-index: 999999999999999999999999;">
        <div class="container" style="margin-top: 15px;">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
              <span class="sr-only">Rozwiń nawigację
              </span>
              <span class="icon-bar">
              </span>
              <span class="icon-bar">
              </span>
              <span class="icon-bar">
              </span>
            </button>
            <a class="navbar-brand" href="logged">
              <img class="img-responsive img-logo" src="img/logo.png">
            </a>
          </div>
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right" style="background: white; z-index: 9999;    margin-top: -2px;">
              <li>
                <a href="logged">
                  <i class="fa fa-home" aria-hidden="true">
                  </i> Strona Główna
                </a>
              </li>
              <li class="dropdown">             
                <a href="#" class="zaloguj" data-toggle="dropdown"><?php echo $_SESSION['User']->Get_login(); ?>
                  <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" role="menu" style="padding: 15px;font-family: sans-serif; font-weight: bold;min-width: 400px;">
                <div style="background:#f7f7f7;float: left; padding: 10px;width: 100%;border: 1px solid #D5DDE5;">
                  <?php
                            echo "<img src='".$_SESSION['User']->Get_avatar()."'  style='min-width: 80px; min-height: 80px; max-width: 80px; max-height: 80px; float: left; border-radius: 40px; padding: 3px; border: 1px solid #D5DDE5;' >";
                            echo "<div style='width: 75%; margin-left: 25%; border: 1px solid #D5DDE5; padding: 5px;'><div style='border-bottom: 1px solid #D5DDE5; padding: 5px 0px;'>Witaj, ". $_SESSION['User']->Get_imie()." ".$_SESSION['User']->Get_nazwisko()."!</div>";
                            echo "<div style='padding: 5px 0px;'>Twój e-mail: ". $_SESSION['User']->Get_email()."</div></div>";                   
                  ?>
                  </div>
                  <div style="background:#f7f7f7;float: left; margin-top: 20px; padding: 10px;width:100%;border: 1px solid #D5DDE5;">
                  Zmień Awatar!
                    <form method="post" action="logged" enctype="multipart/form-data">
                    <input type="hidden" name="size" value="1000000">
                    <div>
                      <input type="file" name="image" accept="image/*">
                    </div>
                    <div>
                      <button type="submit" name="upload">Zatwierdź</button>
                    </div>
                  </form>
                </ul>
              </li>
              <li>
                <a href="logged">
                <form method="post">
                <button style="background: white; border:0;">Wyloguj</button>
                <input type="hidden" name="logout" value="1" />
                </form>
                <?php
                if(@$_POST['logout'])
                    $Strona->Wyloguj();
                ?>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    <div class="container" style="margin-top: 60px; background: white; padding:0; font-family: sans-serif;">    
    <video autoplay muted loop id="myVideo2" class="container" style="padding-right: 0px; padding-left: 0px;">
          <source src="wideo2.mp4" type="video/mp4">
        </video>
    
      <div style="background: #374247;font-size: 1.2em; padding: 15px;" class="menu-section">
<?php
        if($_SESSION['User']->Get_rank()==3)
        {
        ?>
        <a href="/admin">Panel Admina</a>
        <?php
        }
?>
      </div>
                <div style="padding:20px;">
                <div style="padding:0px 30px 20px;border: 1px solid #D5DDE5;">
                <h3>Tworzenie nowego tematu w: <?php echo $Strona->Get_sectionname($_GET['section']); ?> </h3>
                    <form method="post">
                        <label for="temat">Temat:</label><br>
                        <input class="form-control" type="text" size="60" maxlength="150" name="temat" class="newTopic" style="width:100%;"><br>
                        <label for="temat">Treść:</label><br>
                        <textarea class="form-control" rows="10" cols="100" size="100" wrap="physical" name="tresc" class="newTopic" style="width:100%;resize:vertical;"></textarea><br>
                        <input type="submit" name="okej" value="Wyślij" class="btn" style="a"/>
                    </form><b>
<?php 
 if (@$_POST['okej'])
 {
    if(trim($_POST['temat']) == "")
      echo "<font color='red'>Temat nie może być pusty!</font>";
    else
    {
      if(trim($_POST['tresc']) == "")
        echo "<font color='red'>Treść nie może być pusta!</font>";
      else
      {
        $temat = mysqli_real_escape_string($Strona->DB->get_polaczenie(),htmlentities($_POST['temat']));
        $tresc = mysqli_real_escape_string($Strona->DB->get_polaczenie(),htmlentities($_POST['tresc']));
        $Strona->Post($_SESSION['User']->Get_id(), $tresc, $Strona->Get_tablename($_GET['section']),$temat);
        header('Location: logged?section='.$_GET['section']);
        exit();
      }
    }
 }
?></b>
                </div>
                </div>
      </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js">
    </script>
    <script src="js/bootstrap.min.js">
    </script>
    <?php
    ?>
  </body>
  </html>