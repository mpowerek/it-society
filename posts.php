<?php
ob_start();
require_once "Strona.php";
$Strona = new Strona();
define("domena", "localhost");

if (!isset($_SESSION['autologin']))
    {
      header('Location: index');
      exit();
    }

    if(isset($_POST['upload']))
    {
        $check = getimagesize($_FILES["image"]["tmp_name"]);
        if($check !== false) 
        {
          if($_SESSION['User']->Get_avatar() != 'avatars/avatar.jpg')
          {
            unlink($_SESSION['User']->Get_avatar());
          }
          $Strona->DB->Put_avatarintodb($_FILES['image']['name']);
        } else {
            $_SESSION['blad'] = "Plik nie jest obrazem!";
        }
    }
    $wiersz = $Strona->DB->Get_valuefromtablebyid($Strona->Get_tablename($_GET['section']),$_GET['id']);
    $FromTableById = $Strona->DB->Get_valuefromtablebyid('users',$wiersz['iduser'],'id');

    if(@$_GET['delete'] == 'true' && $_SESSION['User']->Get_rank() == 3)
    {
      for($i=1;$i<=$wiersz['ileodp'];$i++)
      {
        $uzytkownik = $Strona->DB->Get_valuefromtablebyid($wiersz['kategoria'].'.'.$wiersz['id'],$i);
        $Strona->DB->decrement_value('users','posts',$uzytkownik['iduser']);
      }
            if($wiersz['ileodp']!=0)
                    $Strona->DB->Delete_table($wiersz['kategoria'].'.'.$wiersz['id']);
            $Strona->DB->Delete_fromtablebyid($wiersz['kategoria'], $wiersz['id'] ,'id');
            $Strona->DB->decrement_value('users','posts',$wiersz['iduser']);
            header('Location: logged?section='.$_GET['section']);
            exit();
    }
    else if(@$_GET['close'] == 'true' && $_SESSION['User']->Get_rank() == 3)
    {
            $Strona->DB->Update_Dbfromtablebyid($wiersz['kategoria'],'close',!$wiersz['close'],$wiersz['id']);
            header('Location: logged?section='.$_GET['section']);
            exit();
    }
    else if(@$_GET['save'] == 'true' && $_SESSION['User']->Get_rank() == 3)
    {
            $Strona->DB->Update_Dbfromtablebyid($wiersz['kategoria'],'save',!$wiersz['save'],$wiersz['id']);
            header('Location: logged?section='.$_GET['section']);
            exit();
    }
    else if(isset($_POST['delete2']) && $_SESSION['User']->Get_rank() == 3)
    {
      $Strona->DB->Delete_fromtablebyid($_POST['delete2'], $_POST['del'] ,'id');
      $Strona->DB->decrement_value($wiersz['kategoria'],'ileodp', $wiersz['id']);
      $Strona->DB->decrement_value('users','posts',$wiersz['iduser']);
          header('Location: #');
          exit();
    }
    
?>
  <!DOCTYPE html>
  <html lang="pl_PL">

  <head>
    <link href="https://fonts.googleapis.com/css?family=Alegreya+Sans+SC" rel="stylesheet">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>IT SOCEITY
    </title>
      <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
      crossorigin="anonymous">
    <script type="text/javascript">
  var js_variable = <?php echo json_encode($_SESSION['autologin']); ?>;
  if(js_variable==false)
  {
    var x = document.referrer;    
    x = x.replace("http://", "");
    var i = x.indexOf ("/"); 
    x = x.substr(0,i);
    if (x != "<?php echo domena; ?>")
      window.location.href = "index";
  }
    function scroll_to(selector) {
      $('html,body').animate({
        scrollTop: $(selector).offset().top
      }, 1000);
      return false;
    }
    </script>
  </head>
  <body>
    <div class="main">
      <nav class="navbar navbar-default" role="navigation" style="background: white; position: fixed; width:100%;border-radius:0px; margin-top: -80px; z-index: 999999999999999999999999;">
        <div class="container" style="margin-top: 15px;">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
              <span class="sr-only">Rozwiń nawigację
              </span>
              <span class="icon-bar">
              </span>
              <span class="icon-bar">
              </span>
              <span class="icon-bar">
              </span>
            </button>
            <a class="navbar-brand" href="logged">
              <img class="img-responsive img-logo" src="img/logo.png">
            </a>
          </div>
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right" style="background: white; z-index: 9999;    margin-top: -2px;">
              <li>
                <a href="logged">
                  <i class="fa fa-home" aria-hidden="true">
                  </i> Strona Główna
                </a>
              </li>
              <li class="dropdown">             
                <a href="#" class="zaloguj" data-toggle="dropdown"><?php echo $_SESSION['User']->Get_login(); ?>
                  <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" role="menu" style="padding: 15px;font-family: sans-serif; font-weight: bold;min-width: 400px;">
                <div style="background:#f7f7f7;float: left; padding: 10px;width: 100%;border: 1px solid #D5DDE5;">
                  <?php
                            echo "<img src='".$_SESSION['User']->Get_avatar()."'  style='min-width: 80px; min-height: 80px; max-width: 80px; max-height: 80px; float: left; border-radius: 40px; padding: 3px; border: 1px solid #D5DDE5;' >";
                            echo "<div style='width: 75%; margin-left: 25%; border: 1px solid #D5DDE5; padding: 5px;'><div style='border-bottom: 1px solid #D5DDE5; padding: 5px 0px;'>Witaj, ". $_SESSION['User']->Get_imie()." ".$_SESSION['User']->Get_nazwisko()."!</div>";
                            echo "<div style='padding: 5px 0px;'>Twój e-mail: ". $_SESSION['User']->Get_email()."</div></div>";
                                    
                  ?>
                  </div>
                  <div style="background:#f7f7f7;float: left; margin-top: 20px; padding: 10px;width:100%;border: 1px solid #D5DDE5;">
                  Zmień Awatar!
                    <form method="post" action="logged" enctype="multipart/form-data">
                    <input type="hidden" name="size" value="1000000">
                    <div>
                      <input type="file" name="image" accept="image/*">
                    </div>
                    <div>
                      <button type="submit" name="upload">Zatwierdź</button>
                    </div>
                  </form>
                </ul>
              </li>
              <li>
                <a href="logged">
                <form method="post">
                <button style="background: white; border:0;">Wyloguj</button>
                <input type="hidden" name="logout" value="1" />
                </form>
                <?php
                if(@$_POST['logout'])
                    $Strona->Wyloguj();
                ?>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    <div class="container" style="margin-top: 60px; background: white; padding:0; font-family: sans-serif;    background: #f3f3f3;">
    
    <video autoplay muted loop id="myVideo2" class="container mobile-none" style="padding-right: 0px; padding-left: 0px;">
          <source src="wideo2.mp4" type="video/mp4">
        </video>
    
      <div style="background: #374247;font-size: 1.2em; padding: 15px;" class="menu-section">
<?php
        if($_SESSION['User']->Get_rank()==3)
        {
        ?>
        <a href="/admin">Panel Admina</a>
        <?php
        }
?>
        &nbsp;
      </div>
                <div style="padding:20px;">
                <div style="padding:20px;border: 1px solid #D5DDE5;">        
                <a href="logged?section=<?php echo $_GET['section'];?>"><button type="button" class="btn btn-default"><?php echo $Strona->Get_sectionname($_GET['section']);?></button></a> <i class="fas fa-arrow-right"></i> <button disabled type="button" class="btn btn-default"><?php echo $wiersz['temat'];?></button>
                <button disabled type="button" style="float:right;" class="btn btn-default">Napisano dnia: <?php echo substr($wiersz['data'],0,19);?></button><br><br>
<div class="row" style="background: white;">
        <div class="col-lg-2" style="border-top: 40px solid #D5DDE5;text-align:center;padding:15px;min-height: 300px;background: white;">
        <img src='<?php echo $FromTableById['avatar'];?>' class="img-thumbnail"><br>
        <h4><?php echo $FromTableById['login'];?></h4><?php echo $Strona->Get_rankname($FromTableById['rank']);?> <br>Napisane posty: <?php echo $FromTableById['posts'];?>
        </div>
    <div class="col-lg-10" style="padding: 15px;border-top: 40px solid #D5DDE5;border-left: 1px solid #D5DDE5;border-right: 1px solid #D5DDE5;min-height: 305.33px;background: white;">
      <textarea class="wyswietlanie" style="width: 100%;" wrap="physical" name="tresc" disabled><?php echo $wiersz['tresc']; ?></textarea></div>
    <div class="col-lg-12" style="padding: 15px;border-left: 1px solid #D5DDE5;border-top: 1px solid #D5DDE5;border-right: 1px solid #D5DDE5;border-bottom: 5px solid #D5DDE5;background: white;">
    <?php
    if(($Strona->Get_data_roznica($wiersz['data']) < 30) && $_SESSION['User']->Get_id() == $wiersz['iduser'])
      echo "<a href='edit?id=".$wiersz['id']."&section=".$_GET['section']."'><button class='btn' style='margin-right:10px;'>Edytuj</button></a>";
    if($_SESSION['User']->Get_rank() == 3)
      echo "<a href='posts?id=".$wiersz['id']."&section=".$_GET['section']."&delete=true'><button class='btn' style='margin-right:10px;'>Usuń</button></a>";
    ?>
    </div></div>
<br><br>
<?php
$tab = $wiersz['kategoria'].'.'.$wiersz['id'];
$sql  =  "SELECT * FROM `$tab`" ;
if($wiersz['ileodp']!=0)
{
        if ( $result  =  mysqli_query ( $Strona->DB->get_polaczenie() , $sql )) 
        {
                if ( mysqli_num_rows ( $result ) >  0 ) 
                {
                        while ( $buf  =  mysqli_fetch_array ( $result )) 
                        {
                            if($FromTableById = $Strona->DB->Get_valuefromtablebyid('users',$buf['iduser'],'id'))
                            {
                                ?>
                                <div class="row">
                                    <div class="col-lg-2" style="border-top: 5px solid #D5DDE5;text-align:center;padding:15px;min-height: 300px;background: white;">
                                    <img src='<?php echo $FromTableById['avatar'];?>' class="img-thumbnail"><br>
                                    <h4><?php echo $FromTableById['login'];?></h4><?php echo $Strona->Get_rankname($FromTableById['rank']);?> <br>Napisane posty: <?php echo $FromTableById['posts'];?>
                                    </div>
                                <div class="col-lg-10" style="padding: 0px;border-top: 5px solid #D5DDE5;border-left: 1px solid #D5DDE5;border-right: 1px solid #D5DDE5;min-height: 300px;background: white;"><div style="background:#f1f1f1;"><p align="right" style="padding: 8px 20px 8px 8px;">Napisano dnia: <?php echo substr($buf['data'],0,19);?></p></div>
                                <div style="width: 100%;padding: 15px;"><textarea class="wyswietlanie" wrap="physical" name="tresc" disabled><?php echo $buf['tresc']; ?></textarea></div>
                                </div>
                                <div class="col-lg-12" style="padding: 15px;border-left: 1px solid #D5DDE5;border-top: 1px solid #D5DDE5;border-right: 1px solid #D5DDE5;border-bottom: 5px solid #D5DDE5;background: white;">
                                <?php
                                if(($Strona->Get_data_roznica($wiersz['data']) < 30) && $_SESSION['User']->Get_id() == $buf['iduser'])
                                  echo "<a href='edit?section=".$_GET['section']."&id=".$_GET['id']."&subid=".$buf['id']."'><button class='btn' style='margin-right:10px;'>Edytuj</button></a>";
                                if($_SESSION['User']->Get_rank() == 3)
                                  echo "<form style='display:inline;' method='post'><input type='hidden' name='delete2' value='".$buf['kategoria']."'/><button class='btn' name='del' value='".$buf['id']."'>Usuń</button></form>";
                                ?>
                                </div></div>  
                            <?php
                            }
                            else
                            {
                              $_SESSION['blad'] = "Błąd zapytania! Nie można pobrać informacji o użytkowniku!". mysqli_error($Strona->DB->get_polaczenie());
                            }
                        }
                        mysqli_free_result($result);
                } 
                else 
                        $_SESSION['blad'] = "Brak Postów!" ;
        } 
        else
        $_SESSION['blad'] = "BŁĄD: nie można wykonać zapytania!." . mysqli_error ( $Strona->DB->get_polaczenie() );
}
if(!$wiersz['close'] || $_SESSION['User']->Get_rank() == 3)
{
?>
<div class="row">
<form method='post'>
<div class="col-lg-12" style="padding: 15px;border-top: 40px solid #D5DDE5;border-left: 1px solid #D5DDE5;border-right: 1px solid #D5DDE5;min-height: 300px;background: white;">
<textarea class="form-control" style="width: 100%;min-height: 230px;resize: vertical;" wrap="physical" name="tresc"></textarea></div>
<div class="col-lg-12" style="padding: 15px;border-left: 1px solid #D5DDE5;border-top: 1px solid #D5DDE5;border-right: 1px solid #D5DDE5;border-bottom: 5px solid #D5DDE5;background: white;">
<button class="btn" name='okej' value='1' style="margin-right:10px;">Wyślij</button></form>
<b>
<?php
}
 if (@$_POST['okej'])
 {
    if(trim($_POST['tresc']) == "")
      echo  "<font color='red'>Treść nie może być pusta!</font>";
    else
    {
      $tresc = mysqli_real_escape_string($Strona->DB->get_polaczenie(),htmlentities($_POST['tresc']));
      $Strona->Create_Post_remesage($wiersz['kategoria'],$wiersz['id'], $tresc);
      header('Location: posts?id='.$_GET['id'].'&section='.$_GET['section']);
      exit();
    }
 }
?></b>
</div>
</div>
      </div>
    </div>
    <script type="text/javascript">
  $('.wyswietlanie').each(function () {
    this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;resize: none;background: none; border: 0px;width: 100%;');
  }).on('input', function () {
    this.style.height = 'auto';
    this.style.height = (this.scrollHeight) + 'px';
});
    </script>
    <script src="js/bootstrap.min.js">
    </script>
  </body>
  </html>