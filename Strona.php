<head>
<script defer src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
</head>
<?php
mb_internal_encoding("UTF-8");
require_once "User.php";
session_start();
require_once "Db.php";

class Strona
{
    public $DB;
    private $RankArray = array(0 => 'Użytkownik',1 =>'Pracodawca',2 =>'Moderator',3 =>'Developer');
    private $SectionArray = array(0 => 'Dla początkujących',1 =>'Dla uczniów',2 =>'Oferty pracy',3 =>'Szukam pracy');
    private $TableArray = array(0 => 'dlalaikow',1 =>'dlauczniow',2 =>'jobs',3 =>'looking');
    
    public function __construct() 
    {
        $this->DB = new Db();
    }

    function Get_rankname($index)
    {
        return $this->RankArray[$index];
    }
    
    function Get_sectionname($index)
    {
        return $this->SectionArray[$index];
    }
    
    function Get_tablename($index)
    {
        return $this->TableArray[$index];
    }

    public function Zarejestruj(&$login,&$email,&$imie,&$nazwisko)
    {  
        $seckey = "6LeFM0gUAAAAACz2vgHOJ6omhVTj0_frZknnN7O4";
        $sprawdz = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$seckey.'&response='.$_POST['g-recaptcha-response']);
        $odpowiedz = json_decode($sprawdz);
        if(!$odpowiedz->success)
        {
            $_SESSION['blad-zarejestruj'] = "Potwierdź że nie jesteś botem! ";
            header('Location: index');
            exit();
        }
        else
        {
            if (isset($_POST['regulamin']))
            {   
                $emailB = filter_var($email, FILTER_SANITIZE_EMAIL);
                if (!filter_var($emailB, FILTER_VALIDATE_EMAIL) || ($email!=$emailB))
                {
                    $_SESSION['blad-zarejestruj'] = "Niepoprawny adress email! ";
                    header('Location: index');
                    exit();
                }
                else
                {
                    if($idzapytania = $this->DB->get_polaczenie()->query("SELECT * from `users` WHERE `email` = '$email'"))
                    {
                        if ($wiersz = $idzapytania->fetch_assoc())
                        { 
                            $_SESSION['blad-zarejestruj'] = "Ten e-mail jest już w użyciu! ";
                            header('Location: index');
                            exit();
                        }
                        else 
                        {
                            if($idzapytania = $this->DB->get_polaczenie()->query("SELECT * from `users` WHERE `login` = '$login'"))
                            {
                                if ($wiersz = $idzapytania->fetch_assoc()) 
                                {
                                    $_SESSION['blad-zarejestruj'] = "Ten login jest zajęty! ";
                                    header('Location: index');
                                    exit();
                                }
                                else 
                                {
                                    if(strlen($_POST['register_password']) < 1)
                                    {
                                        $_SESSION['blad-zarejestruj'] = "Hasło jest zbyt krótkie (min. 8znakow) ";
                                        header('Location: index');
                                        exit();
                                    }
                                    else
                                    {
                                        if ($_POST['register_password'] != $_POST['register_password2'])
                                        {
                                            $_SESSION['blad-zarejestruj'] = "Wpisane hasła różnią się od siebie! ";
                                            header('Location: index');
                                            exit();
                                        }
                                        else
                                        {
                                            $pass = password_hash($_POST['register_password'], PASSWORD_DEFAULT);
                                            $zapytanie = "INSERT INTO `users` (`id`, `login`, `pass`, `email`,`imie`,`nazwisko`, `rank`, `avatar` ) VALUES ('', '$login', '$pass', '$email','$imie','$nazwisko', '0', 'avatars/avatar.jpg')";
                                            $idzapytania = $this->DB->get_polaczenie()->query($zapytanie);
                                            $_SESSION['blad-zarejestruj'] = "Zarejestrowano Pomyślnie! ";
                                            header('Location: index');
                                            exit();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                $_SESSION['blad-zarejestruj'] = "Musisz zaakceptowac regulamin! ";
                header('Location: index');
                exit();
            }
        }
    }

    public function Zaloguj(&$login)
    {
        if (!$login or empty($login))
        {
            $_SESSION['blad-zaloguj'] = "Wypełnij pole z loginem! ";
            header('Location: index');
            exit(); 
        }
        if (!$_POST['password'] or empty($_POST['password']))
        {
            $_SESSION['blad-zaloguj'] = "Wypełnij pole z hasłem! ";
            header('Location: index');
            exit();
        }
        if($idzapytania = $this->DB->get_polaczenie()->query("SELECT * from `users` WHERE `login` = '$login'"))
        {
            if ($wiersz = $idzapytania->fetch_assoc()) 
            {
                if(!password_verify($_POST['password'],$wiersz['pass']))
                {
                    $_SESSION['blad-zaloguj'] = "Nieprawidłowe hasło! ";
                    header('Location: index');
                    exit();
                }
                else
                {
                    $_SESSION['User'] = (object)new User($wiersz);
                    $idzapytania->free_result();
                    if(isset($_POST['autologin']))
                        $_SESSION['autologin'] = true;
                    else
                        $_SESSION['autologin'] = false; 
                    header('Location: logged');
                    exit();
                }
            }
            else 
            {
                $_SESSION['blad-zaloguj'] = "Użytkownik o podanym loginie nie istnieje! ";
                header('Location: index');
                exit();
            }
        }
        else
        {
            $_SESSION['blad-zaloguj'] =  "BŁĄD: nie można wykonać zapytania!." . mysqli_error ( $this->DB->get_polaczenie() );    
            header('Location: index');
            exit();
        }
    }

    public function Wyloguj()
    {
        session_unset();
        session_destroy();
        header('Location: index');
        exit();
    }

    public function Post($iduser,&$tresc,$tabela,$temat=" ")
    {
        if (!$temat or empty($temat)) 
        {
            $_SESSION['blad'] = "Wypełnij pole z tematem! ";
        }
       if (!$tresc or empty($tresc)) 
       {
           $_SESSION['blad'] = "Wypełnij pole z treścią! ";
       }
       $czas = $this->Get_data();
       if ($idzapytania = $this->DB->get_polaczenie()->query("INSERT INTO `$tabela` (`id`, `kategoria` , `iduser`, `temat`, `tresc`, `data`, `dataedycji` ,`ileodp`, `close`, `save`) VALUES ('', '$tabela' , '$iduser', '$temat', '$tresc', '$czas', '$czas','0','0','0')"))
       {
           $_SESSION['blad'] = "Post został utworzony! ";
           $this->DB->increment_value('users','posts',$_SESSION['User']->Get_id());
       }
       else
           $_SESSION['blad'] = "Nie udało się utworzyć postu! ";
    }

    function Get_posts($jakie)
    {
        $na_strone = 20;
        $sql  =  "SELECT * FROM `$jakie`";
        $ile = 0;
        if ( $result  =  mysqli_query ( $this->DB->get_polaczenie() , $sql )) 
        {
            $rows = mysqli_num_rows ( $result );
            if ( $rows >  0 ) 
            {
                $liczba_stron = ceil($rows / $na_strone);
                if (isset($_GET['strona']))
                {
                    if($_GET['strona'] < 1 || $_GET['strona'] > $liczba_stron)
                        $strona = $_GET['strona'] = 1;
                    else
                        $strona = $_GET['strona'];
                }
                else 
                    $strona = $_GET['strona']=1;
                $od = $na_strone * ($strona-1);
                $sql  =  "SELECT * FROM `$jakie` ORDER BY `save` DESC, `dataedycji` DESC LIMIT $od , $na_strone";
                if ( $result  =  mysqli_query ( $this->DB->get_polaczenie() , $sql ))
                {
                    while ( $wiersz  =  mysqli_fetch_array ( $result )) 
                    {
                        $przypiety = "";
                        $user = $this->DB->Get_valuefromtablebyid('users',$wiersz['iduser'],'id');
                        if($wiersz['save'])
                            $przypiety = "<div style='background: #4caf50; float: left; color: white; font-size: 10px; padding: 4px 10px; border-radius: 6px;'>Przypięty</div>";
                        $ile++;

                        if(!$wiersz['close'])
                            $style = "style='color:#d4d4d4;'";
                        else
                            $style="";
                        echo "<div class='container'>
                              <div class='row' style='border-bottom: 1px solid #D5DDE5;border-top: 1px solid #D5DDE5;padding:10px;'>
                              <a href='posts?id=".$wiersz['id']."&section=".$_GET['section']."'>
                              <div class='col-lg-1 thread-icon'><i class='fas fa-lock' ".$style."></i></div>
                              <div class='col-lg-6 thread-title'><b>".$przypiety.$wiersz['temat']."</b></div>
                              <div class='col-lg-1 thread-answers'><b>".$wiersz['ileodp']."</b> Odpowiedzi</div>
                              <div class='col-lg-1 thread-image'><img src='".$user['avatar']."' style=''></div>
                              <div class='col-lg-2 thread-date'>Napisano:<br><b>".substr($wiersz['data'],0,19)."</b><br>przez: <b>".$user['login']."</b></div></a>";
                        if(@$_SESSION['User']->Get_rank() == 3)
                        {
                            echo "<div class='col-lg-1 thread-admin'>
                                    <a href='posts?id=".$wiersz['id']."&section=".$_GET['section']."&save=true'><i class='fas fa-thumbtack'></i></a>
                                    <a href='posts?id=".$wiersz['id']."&section=".$_GET['section']."&close=true'><i class='fas fa-lock'></i></a>
                                    <a href='posts?id=".$wiersz['id']."&section=".$_GET['section']."&delete=true'><i class='fas fa-times-circle'></i></a>
                                    </div>";
                        }
                        echo "</div></div>";
                    }
                    mysqli_free_result($result);
                    return $liczba_stron;
                }
            } 
            else 
                echo  "Brak Postów!" ;
        } 
        else
            echo  "BŁĄD: nie można wykonać zapytania!." . mysqli_error ( $this->DB->get_polaczenie() );
    }

    function Create_Post_remesage($table,&$id,&$tresc)
    {
        if ($idzapytania = $this->DB->get_polaczenie()->query("
        CREATE TABLE IF NOT EXISTS `$table.$id` (
            id int(11) NOT NULL AUTO_INCREMENT,
            kategoria varchar(20) NOT NULL,
            iduser int(11) NOT NULL,
            temat varchar(100) NOT NULL,
            tresc varchar(8192) NOT NULL,
            data datetime(6) NOT NULL,
            dataedycji datetime(6) NOT NULL,
            ileodp int(11) NOT NULL,
            close tinyint(1) NOT NULL,
            save tinyint(1) NOT NULL,
            PRIMARY KEY (id)
            ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
        ")) 
        {
            if ($this->DB->increment_value($table,'ileodp', $id))
                if($this->DB->Update_Dbfromtablebyid($table,'dataedycji',$this->Get_data(),$id)) 
                    $this->Post($_SESSION['User']->Get_id(),$tresc, $table.'.'.$id);
            else 
            $_SESSION['blad'] =  "BŁĄD: nie można wykonać zapytania!." . mysqli_error ( $this->DB->get_polaczenie() );
        }  
        else 
        $_SESSION['blad'] =  "BŁĄD: nie można wykonać zapytania!." . mysqli_error ( $this->DB->get_polaczenie() );
    }
    
    public function Get_data()
    {
        return date('Y-m-d H:i:s');
    }

    public function Get_data_roznica($czas)
    {
        $od = strtotime($czas);
        $do = strtotime($this->Get_data());
        $roznica = abs($od - $do);
        $roznica_minuty = floor($roznica / 60);
        return $roznica_minuty;
    }

    public function Get_usersbyid($wartosc="",$poczym="")
    {
        $na_strone = 20;
        if($wartosc == "")
            $sql  =  "SELECT * FROM `users`";
        else
            $sql  =  "SELECT * FROM `users` WHERE `$poczym` = '$wartosc'";
        $ile = 0;
        if ( $result  =  mysqli_query ( $this->DB->get_polaczenie() , $sql )) 
        {
            $rows = mysqli_num_rows ( $result );
            if ( $rows >  0 ) 
            {
                $liczba_stron = ceil($rows / $na_strone);
                if (isset($_GET['strona']))
                {
                    if($_GET['strona'] < 1 || $_GET['strona'] > $liczba_stron)
                        $strona = $_GET['strona'] = 1;
                    else
                        $strona = $_GET['strona'];
                }
                else 
                    $strona = $_GET['strona']=1;
                $od = $na_strone * ($strona-1);
                if($wartosc == "")
                    $sql  =  "SELECT * FROM `users` LIMIT $od , $na_strone";
                else
                    $sql  =  "SELECT * FROM `users` WHERE `$poczym` = '$wartosc' LIMIT $od , $na_strone";
                if ( $result  =  mysqli_query ( $this->DB->get_polaczenie() , $sql ))
                {
                    while ( $wiersz  =  mysqli_fetch_array ( $result )) 
                    {
                       echo "<div class='row' style='border-bottom: 1px solid #D5DDE5;border-top: 1px solid #D5DDE5;padding:10px;'>
                        <div class='col-lg-1' >".$wiersz['id']."</div>
                        <div class='col-lg-2' >".$wiersz['login']."</div>
                        <div class='col-lg-2' >".$wiersz['email']."</div>
                        <form method='post'><div class='col-lg-2' >
                              <select name='setrank'>";
                              for($i=0;$i<4;$i++)
                              {
                                  if($wiersz['rank'] == $i)
                                    echo "<option value='".$i."' selected>    ".$this->Get_rankname($i)."    </option>";
                                  else
                                    echo "<option value='".$i."'>    ".$this->Get_rankname($i)."    </option>";
                              }
                        echo "</select>
                        </div>
                        <div class='col-lg-2' >".$wiersz['imie']."</div>
                        <div class='col-lg-2' >".$wiersz['nazwisko']."</div>
                        <div class='col-lg-1'>
                                    <button class='btn-link' name='delete' value=".$wiersz['id']."><i class='fas fa-times-circle'></i></button></form>
                                    <button class='btn-link' name='id' value=".$wiersz['id']."><i class='fas fa-save'></i></button>
                        </div>
                      </div>";
                    }
                    mysqli_free_result($result);
                    return $liczba_stron;
                }
            } 
            else 
                echo  "Brak Użytkowników!" ;
        } 
        else
            echo  "BŁĄD: nie można wykonać zapytania!." . mysqli_error ( $this->DB->get_polaczenie() );
    }

    function Get_pager($liczbastron)
    {
        if(isset($liczbastron))
        {
            if($_GET['strona'] == 1)
            echo "<div style='margin-left: 30;margin-right: 30;'><ul class='pager'><li class='previous disabled'>
            <a href='#'><span aria-hidden='true'>&larr;</span> Older</a></li>";
            else
            echo "<div style='margin-left: 30;margin-right: 30;'><ul class='pager'><li class='previous'>
            <a href='?section=".$_GET['section']."&strona=".($_GET['strona']-1)."'><span aria-hidden='true'>&larr;</span> Older</a></li>";
            if($_GET['strona'] == $liczbastron)
            echo "<li class='next disabled'><a href='#'>Newer 
            <span aria-hidden='true'>&rarr;</span></a></li></ul></div>";
            else
            echo "<li class='next'><a href='?section=".$_GET['section']."&strona=".($_GET['strona']+1)."'>Newer 
            <span aria-hidden='true'>&rarr;</span></a></li></ul></div>";
            echo "<center>";
            echo "<nav>";
        echo "<ul style='margin: -50px;' class='pagination'>";
        if($_GET['strona'] < 3)
            $Poczatek = 1;
        else if ($_GET['strona'] > $liczbastron-2)
            $Poczatek = $liczbastron-4;
        else
            $Poczatek = $_GET['strona']-2;
        if($_GET['strona'] == 1)
            echo "<li class='disabled'><a href='#' aria-label='Previous'><span aria-hidden='true'>&laquo;</span></a></li>";
        else
            echo "<li><a href='?section=".$_GET['section']."&strona=1' aria-label='Previous'><span aria-hidden='true'>&laquo;</span></a></li>";
        for($i=$Poczatek;$i<($Poczatek+5);$i++)
        {
            if($i == $_GET['strona'])
            echo "<li class='active'><a href='?section=".$_GET['section']."&strona=".$i."'>".$i." <span class='sr-only'>(current)</span></a></li>";
            else
            echo "<li><a href='?section=".$_GET['section']."&strona=".$i."'>".$i."</a></li>";
            if($i == $liczbastron)
            break;
        }
        if($_GET['strona'] == $liczbastron)
            echo "<li class='disabled'><a href='#' aria-label='Next'><span aria-hidden='true'>&raquo;</span></a></li>";
        else
            echo "<li><a href='?section=".$_GET['section']."&strona=".$liczbastron."' aria-label='Next'><span aria-hidden='true'>&raquo;</span></a></li>";
            echo '</ul>';
            echo '</nav>';
            echo '</center>';
        }
    }
}
?>