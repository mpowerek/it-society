﻿<?php
///////////////////////
// RANGI
// 0 - uzytkownik
// 1 - pracodawca
// 2 - moderator
// 3 - developer
///////////////////////


class User
{
    public $id;
    public $login;
    public $imie;
    public $nazwisko;
    public $email;
    public $pass;
    public $rank;
    public $avatar;
    
    public function __construct($tab)
    {
        $this->id = $tab['id'];
        $this->login = $tab['login'];
        $this->pass = $tab['pass'];
        $this->email = $tab['email'];
        $this->imie = $tab['imie'];
        $this->nazwisko = $tab['nazwisko'];
        $this->rank = $tab['rank'];
        $this->avatar = $tab['avatar'];
    }

    public function Get_login()
    {
        return $this->login;
    }
    public function Get_id()
    {
        return $this->id;
    }
    public function Get_imie()
    {
        return $this->imie;
    }
    public function Get_nazwisko()
    {
        return $this->nazwisko;
    }
    public function Get_email()
    {
        return $this->email;
    }
    public function Get_rank()
    {
        return $this->rank;
    }

    public function Get_avatar()
    {
        return $this->avatar;
    }

    public function Change_avatar($image)
    {
        $this->avatar = $image;
    }
}
?>