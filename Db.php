<?php

class Db
{
    private $host = 'localhost';
    private $db_user = 'root';
    private $db_password = '';
    private $db_name = 'itsociety';
    private $polaczenie;

    public $User;
    
    public function __construct() 
    {
        $this->polaczenie = @new mysqli($this->host,$this->db_user,$this->db_password,$this->db_name);
        mysqli_set_charset( $this->polaczenie,'utf8');
        if($this->polaczenie->connect_errno!=0)
        {
            $_SESSION['blad'] = "Error: ".$this->polaczenie->connect_errno;
            exit();
        }
    }

    public function __destruct() 
    {
        $this->polaczenie->close();
    }

    public function get_polaczenie()
    {
        return $this->polaczenie;
    }

    function Put_avatarintodb($image)
    {
        $target = "avatars/".basename($image);
        $user = $_SESSION['User']->Get_id();
        if($idzapytania = $this->polaczenie->query("UPDATE `users` SET `avatar`='$target' WHERE id = '$user' "))
        {
          if (move_uploaded_file($_FILES['image']['tmp_name'], $target)) 
          {
              $_SESSION['User']->Change_avatar($target);
              $_SESSION['blad'] = "Image uploaded successfully";
          }
          else
          {
              $_SESSION['blad'] = "Failed to upload image";
          }
        }
        else
            $_SESSION['blad'] = "BŁĄD: nie można wykonać zapytania!." . mysqli_error ( $this->polaczenie ); 
    }

    function Delete_fromtablebyid($table,$wherevalue,$id="id")
    {
        if ($this->polaczenie->query("DELETE FROM `$table` WHERE `$id` = '$wherevalue'")) 
        {}  
        else 
        $_SESSION['blad'] = "BŁĄD: nie można wykonać zapytania!." . mysqli_error ( $this->polaczenie );
    }

    function Get_valuefromtablebyid($table,$wherevalue,$id="id")
    {
        if ($idzapytania = $this->polaczenie->query("SELECT * FROM `$table` WHERE `$id` = '$wherevalue'")) 
        {
            if ($wiersz = $idzapytania->fetch_assoc()) 
            {
                    $idzapytania->free_result();
                    return $wiersz; 
            }
            else 
            {
                $_SESSION['blad'] = "Nie znaleziono szukanego wpisu! ";
                return false;
            }
        }  
        else 
        $_SESSION['blad'] = "BŁĄD: nie można wykonać zapytania!." . mysqli_error ( $this->polaczenie );
    }

    function increment_value($table,$value,$id)
    {
        if ($this->polaczenie->query("UPDATE `$table` SET `$value`=`$value`+1 WHERE id = '$id' ")) 
            return true;
        else 
        {
            $_SESSION['blad'] = "BŁĄD: nie można wykonać zapytania! " . mysqli_error ( $this->polaczenie );
            return false;
        }
    }

    function decrement_value($table,$value,$id)
    {
        if ($this->polaczenie->query("UPDATE `$table` SET `$value`=`$value`-1 WHERE id = '$id' ")) 
            return true;
        else 
        {
            $_SESSION['blad'] = "BŁĄD: nie można wykonać zapytania! " . mysqli_error ( $this->polaczenie );
            return false;
        }
    }

    function Delete_table($table)
    {
        if ($this->polaczenie->query("DROP TABLE `$table`")) 
            return true;
        else 
        {
            $_SESSION['blad'] = "BŁĄD: nie można wykonać zapytania! " . mysqli_error ( $this->polaczenie );
            return false;
        }
    }

    public function Update_Dbfromtablebyid($table,$row,$value,$id)
    {
        if ($this->polaczenie->query("UPDATE `$table` SET `$row`='$value' WHERE id = '$id' ")) 
            return true;
        else 
        {
           echo $_SESSION['blad'] = "BŁĄD: nie można wykonać zapytania! " . mysqli_error ( $this->polaczenie );
            return false;
        }
    }
}
?>