<?php
ob_start();
require_once "Strona.php";
$Strona = new Strona();
define("domena", "localhost");

if (!isset($_SESSION['autologin']))
    {
      header('Location: index');
      exit();
    }
if(!isset($_GET['section']))
    $_GET['section'] = 0;
if(isset($_POST['upload']))
{
    if(getimagesize($_FILES["image"]["tmp_name"])) 
    {
      if($_SESSION['User']->Get_avatar() != 'avatars/avatar.jpg')
      {
        unlink($_SESSION['User']->Get_avatar());
      }
      $Strona->DB->Put_avatarintodb($_FILES['image']['name']);
    } else {
        $_SESSION['blad'] = "Plik nie jest obrazem!";
    }
}  
?>
  <!DOCTYPE html>
  <html lang="pl_PL">
  <head>
    <link href="https://fonts.googleapis.com/css?family=Alegreya+Sans+SC" rel="stylesheet">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>IT SOCEITY
    </title>
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
      crossorigin="anonymous">
    <script type="text/javascript">
  var js_variable = <?php echo json_encode($_SESSION['autologin']); ?>;
  if(js_variable==false)
  {
    var x = document.referrer;    
    x = x.replace("http://", "");
    var i = x.indexOf ("/"); 
    x = x.substr(0,i);
    if (x != "<?php echo domena; ?>")
      window.location.href = "index";
  }
    function scroll_to(selector) {
      $('html,body').animate({
        scrollTop: $(selector).offset().top
      }, 1000);
      return false;
    }
    </script>
  </head>
  <body>
    <div class="main">
      <nav class="navbar navbar-default" role="navigation" style="background: white; position: fixed; width:100%;border-radius:0px; margin-top: -80px; z-index: 999999999999999999999999;">
        <div class="container" style="margin-top: 15px;">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
              <span class="sr-only">Rozwiń nawigację
              </span>
              <span class="icon-bar">
              </span>
              <span class="icon-bar">
              </span>
              <span class="icon-bar">
              </span>
            </button>
            <a class="navbar-brand" href="#">
              <img class="img-responsive img-logo" src="img/logo.png">
            </a>
          </div>
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right" style="background: white; z-index: 9999;    margin-top: -2px;">
              <li>
                <a href="#">
                  <i class="fa fa-home" aria-hidden="true">
                  </i> Strona Główna
                </a>
              </li>
              <li class="dropdown">
                <a href="#" class="zaloguj" data-toggle="dropdown"><?php echo $_SESSION['User']->Get_login(); ?>
                  <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" role="menu" style="padding: 15px;font-family: sans-serif; font-weight: bold;min-width: 400px;">
                <div style="background:#f7f7f7;float: left; padding: 10px;width: 100%;border: 1px solid #D5DDE5;">
                  <?php
                            echo "<img src='".$_SESSION['User']->Get_avatar()."'  style='min-width: 80px; min-height: 80px; max-width: 80px; max-height: 80px; float: left; border-radius: 40px; padding: 3px; border: 1px solid #D5DDE5;' >";
                            echo "<div style='width: 75%; margin-left: 25%; border: 1px solid #D5DDE5; padding: 5px;'><div style='border-bottom: 1px solid #D5DDE5; padding: 5px 0px;'>Witaj, ". $_SESSION['User']->Get_imie()." ".$_SESSION['User']->Get_nazwisko()."!</div>";
                            echo "<div style='padding: 5px 0px;'>Twój e-mail: ". $_SESSION['User']->Get_email()."</div></div>";               
                  ?>
                  </div>
                  <div style="background:#f7f7f7;float: left; margin-top: 20px; padding: 10px;width:100%;border: 1px solid #D5DDE5;">
                  Zmień Awatar!
                    <form method="post" enctype="multipart/form-data">
                    <input type="hidden" name="size" value="1">
                    <div>
                      <input type="file" name="image" accept="image/*">
                    </div>
                    <div>
                      <button type="submit" name="upload">Zatwierdź</button>
                    </div>
                  </form>
                </ul>
              </li>
              <li>
                <a href="#">
                <form method="post">
                <button style="background: white; border:0;">Wyloguj</button>
                <input type="hidden" name="logout" value="1" />
                </form>
                <?php
                if(@$_POST['logout'])
                    $Strona->Wyloguj();
                ?>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    <div class="container" style="margin-top: 60px; background: white; padding:0; font-family: sans-serif;">
    <video autoplay muted loop id="myVideo2" class="container telephone-none small-tablet-none average-tablet-none" style="padding-right: 0px; padding-left: 0px;">
          <source src="wideo2.mp4" type="video/mp4">
        </video>   
      <div style="background: #374247;font-size: 1.2em; display: flow-root;" class="menu-section-on-devices">
      <div style="padding: 10px;float:left;" class="menu-section">
<?php
for($i=0;$i<4;$i++)
{
  if($_GET['section'] == $i)
    echo "<a class='menu-active' href='?section=".$i."'>".$Strona->Get_sectionname($i)."</a>";
  else
    echo "<a href='?section=".$i."'>".$Strona->Get_sectionname($i)."</a>";

}    
        if($_SESSION['User']->Get_rank()==3)
        {
        ?>
        <a href="/admin">Panel admina</a>
        <?php
        }
?></div>
        <a href="createpost?section=<?php echo $_GET['section'];?>" style='color:white;font-weight:bold;'>
        <div style="float: right; background: #4caf50; padding: 10px 25px;" class="btn">
        Napisz post!
        </div>
        </a>
      </div>
      <div style="padding: 15px 0px;">
        <div style="padding: 15px;background: #c71234; color: white;">
        </div>
        <div style="padding: 5px;background: #f7f7f7;">
<?php
        $liczbastron = $Strona->Get_posts($Strona->Get_tablename($_GET['section']));
?>
        </div>  
      </div>
      <?php
      $Strona->Get_pager($liczbastron);
      ?>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js">
    </script>
    <script src="js/bootstrap.min.js">
    </script>
  </body>
  </html>